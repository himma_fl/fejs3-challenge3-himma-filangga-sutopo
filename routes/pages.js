const router = require("express").Router();
const page = require("../controllers/pagesController");

router.get("/home", page.home);
router.get("/", page.login);
router.get("/cars", page.cars);
router.get("/new-car", page.new_car);

module.exports = router;