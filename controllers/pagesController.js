exports.home = (req, res) => {
    const title = "Hello World",
        subTitle = "Welcome to the world!";

    res.render("index", { title, subTitle });
};

exports.login = (req, res) => {
    res.render("login", { layout: 'login' });
};

exports.register = (req, res) => {
    res.render("register", {});
};

exports.cars = (req, res) => {
    res.render("cars", {});
};
exports.new_car = (req, res) => {
    res.render("newCar.ejs", {});
};